import * as dotenv from 'dotenv'

dotenv.config()
if (dotenv.error) {
  throw dotenv.error
}

let configs = {
  NODE_PORT: process.env.NODE_PORT,
  MONGO_URL: process.env.MONGO_URL,
}

export default configs
