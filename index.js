import Koa from 'koa'
import logger from 'koa-logger'
import bodyParser from 'koa-bodyparser'
import cors from '@koa/cors'
import configs from './configs/app'
import route from './routes/route'
import { InitializeDB } from './db/db'

// Create Koa app
const startApp = () => {
  const app = new Koa()
  app
    .use(logger())
    .use(bodyParser())
    .use(cors())
    .use(route.routes())
    .use(route.allowedMethods())

  // Start the application
  app
    .listen(configs.NODE_PORT, () => {
      console.log(`Server listening on port: ${configs.NODE_PORT}`)
    })
    .on('Error', err => {
      console.log(err)
    })
}

// 1) InitializeDB
// 2) Start app
InitializeDB(configs).then(startApp)
