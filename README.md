# To-do list API (Node.js with Koa)

A barebones Node.js app using [Koa](https://www.npmjs.com/package/koa).

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and the [Mongo DB](https://www.mongodb.com/)

```sh
$ git clone https://gitlab.com/Anuchattop1/todolist.git
$ cd todolist
$ npm install
$ npm start
```

Your app should now be running on [localhost:8080](http://localhost:8080/)

<!-- ## Deploying to Heroku

```
$ heroku create
$ git push heroku master
$ heroku open
```

or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy) -->

## Project Todolist Structure

```
.
├── confiigs
|   └── app.js
├── controllers
|   ├── createTask.js
|   ├── deleteTaskByID.js
|   ├── index.js
|   ├── listAllTasks.js
|   ├── listTaskByID.js
|   └── updateTaskByID.js
├── model
|   └── task.js
├── node_modules
├── repository
|   ├── index.js
|   └── task.js
├── routes
|   └── route.js
├── .babelrc
├── .env
├── .gitignore
├── docker-compose.yml
├── package.json
└── package-lock.json

```

## List of Packages

| Package        | Description                                           |
| -------------- | ----------------------------------------------------- |
| koa            | Node.js web framework                                 |
| koa-bodyparser | A body parser for koa                                 |
| koa-Logger     | Logger middleware for koa                             |
| koa-router     | Router middleware for koa                             |
| mongoose       | MongoDB ODM                                           |
| prettier       | Prettier is an opinionated code formatter             |
| dotenv         | Loads environment variables from .env file            |
| nodemon        | Automatically restart Node.js server on code changes. |
| @types/jest    | JavaScript ES6                                        |
