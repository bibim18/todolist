import { task } from '../repository/index'

async function updateTaskByID(ctx) {
  let reqBody = ctx.request.body
  const { title, description } = reqBody
  let id = ctx.params.id

  task.updateByID(id, title, description)
  let t = await task.queryByID(id)
  ctx.body = {
    code: 200,
    data: t,
    msg: 'Update task by ID successfully',
  }
}

export default updateTaskByID
