import { Task } from '../model/task'

export async function deleteByID(_id) {
  const filter = { _id }
  return await Task.findOneAndRemove(filter)
}

export async function updateByID(_id, title, description) {
  const filter = { _id }
  const update = { title, description }
  return await Task.findOneAndUpdate(filter, update)
}

// Insert one new `Task` document
export function create(reqBody) {
  const { title, description } = reqBody
  Task.create(
    {
      title,
      description,
    },
    function (err) {
      if (err) return console.log(err)
    },
  )
}

export async function queryAll() {
  return await Task.find()
}

export async function queryByID(_id) {
  return await Task.findById(_id)
}
