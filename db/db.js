import mongoose from 'mongoose'

const preset = () => {
  mongoose.Promise = Promise
  mongoose.connection.on('connected', () => {
    console.log('Connection Established')
  })

  mongoose.connection.on('reconnected', () => {
    console.log('Connection Reestablished')
  })

  mongoose.connection.on('disconnected', () => {
    console.log('Connection Disconnected')
  })

  mongoose.connection.on('close', () => {
    console.log('Connection Closed')
  })

  mongoose.connection.on('error', error => {
    console.log('ERROR: ' + error)
  })
}

export async function InitializeDB(configs) {
  preset()
  // Start DB connection!
  try {
    await mongoose.connect(configs.MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      connectTimeoutMS: 1000,
    })
    console.log('Started connection on ' + configs.MONGO_URL + ', waiting to open...')
  } catch (error) {
    console.log(`Setting failed to connect to ${configs.MONGO_URL}\n`, error.message)
    process.exit(1)
  }
}
